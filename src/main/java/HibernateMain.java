import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;


public class HibernateMain {

    public static void main(String[] args) {

        EmployeeEntity e1 = new EmployeeEntity();
        e1.setEmail("frank.cosme619@gmail.com");
        e1.setFirstName("Frank");
        e1.setLastName("Cosme");


        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the employee objects
            session.save(e1);


        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List < EmployeeEntity > employee = session.createQuery("SELECT e from EmployeeEntity e ", EmployeeEntity.class).list();
            employee.forEach(s -> {System.out.println(s.getFirstName());System.out.println(s.getEmail());System.out.println(s.getLastName());});
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

}